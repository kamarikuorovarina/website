{
  description = "värinä website";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
            pkgs = nixpkgs.legacyPackages.${system};
        in
        {
          devShells.default = pkgs.mkShell rec {
            packages = with pkgs; [
              nodejs_18
              git-lfs
            ];

            PROJECT_ROOT = builtins.getEnv "PWD";
            LD_LIBRARY_PATH = [ "${pkgs.curl.out}/lib" ];

            shellHook = ''
              [ -d node_modules ] || npm i
              echo "Start development by running npm run dev"
            '';
          };
        }
      );
}
