import adapter from '@sveltejs/adapter-static'
import { vitePreprocess } from '@sveltejs/kit/vite'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://kit.svelte.dev/docs/integrations#preprocessors
  // for more information about preprocessors
  preprocess: vitePreprocess(),

  kit: {
    adapter: adapter({ pages: 'public', assets: 'public' }),
    alias: {
      $components: 'src/components',
      $content: 'src/content',
      $collections: 'src/lib/cms/collections',
      $media: 'src/media',
      $styles: 'src/styles'
    },
    prerender: {
      entries: ['/admin', '/'],
      handleHttpError: ({ path, message }) => {
        if (path.startsWith('/thumbnails/')) {
          return
        }

        throw new Error(message)
      }
    }
  }
}

export default config
