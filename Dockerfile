FROM node:lts AS build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
