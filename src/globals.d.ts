import type { Theme } from './lib/tailwind'

declare global {
  const TAILWIND_THEME: Theme
}

export {}
