import { fetchData as fetchHome } from '$collections/pages/home'
import type { PageLoad } from './$types'

export const load: PageLoad = async () => {
  const home = await fetchHome()

  return { home, description: home.introduction }
}
