import { fetchLayoutData } from '$lib/cms/layout-data'
import type { LayoutLoad } from './$types'

export const load: LayoutLoad = async () => {
  return await fetchLayoutData()
}
