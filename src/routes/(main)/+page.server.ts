import { fetchData as fetchHome } from '$collections/pages/home'
import type { PageServerLoad } from './$types'

export const load: PageServerLoad = async () => {
  const home = await fetchHome()

  return home
}
