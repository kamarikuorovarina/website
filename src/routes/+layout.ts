import type { LayoutLoad } from './$types'
import { fetchMetadata } from '$collections/pages/home'

export const prerender = true
export const trailingSlash = 'always'

export const load: LayoutLoad = async () => {
  const { description } = await fetchMetadata()

  return {
    description
  }
}
