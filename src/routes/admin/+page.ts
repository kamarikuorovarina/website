import type { PageLoad } from './$types'

export const ssr = false

export const load: PageLoad = async () => {
  return {
    title: 'Admin',
    description: 'Kamarikuoro Värinän hallinnointi'
  }
}
