import { fetchData } from '$collections/pages/news'
import type { PageLoad } from './$types'
import { news as wave } from '$lib/waves'

export const load: PageLoad = async () => {
  const news = await fetchData()

  return {
    news,
    wave,
    title: news.title
  }
}
