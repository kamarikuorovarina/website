import { fetchData } from '$collections/news'
import type { PageServerLoad } from './$types'
import { news as wave } from '$lib/waves'
import { error } from '@sveltejs/kit'

export const load: PageServerLoad = async ({ params }) => {
  const news = await fetchData(true)
  const newsItem = news.find(({ slug }) => slug === params.slug)

  if (!newsItem) {
    throw error(404, 'News item not found')
  }

  return {
    newsItem,
    wave
  }
}
