import type { PageLoad } from './$types'

export const load: PageLoad = async ({ parent, data }) => {
  await parent()

  return {
    ...data,
    title: data.newsItem.title,
    description: data.newsItem.excerpt,
    image: data.newsItem.ogImage
  }
}
