import { fetchData } from '$collections/pages/concerts'
import { concerts as wave } from '$lib/waves'
import type { PageLoad } from './$types'

export const load: PageLoad = async () => {
  const concerts = await fetchData()

  return {
    concerts,
    wave,
    title: concerts.title
  }
}
