import type { PageServerLoad } from './$types'
import { press as wave } from '$lib/waves'
import { fetchData } from '$lib/server/press'

export const load: PageServerLoad = async ({ fetch }) => {
  const press = await fetchData(fetch)

  return {
    press,
    wave
  }
}
