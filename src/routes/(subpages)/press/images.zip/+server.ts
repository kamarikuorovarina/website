import type { RequestHandler } from './$types'
import archiver from 'archiver'
import { basename } from 'path'
import { PassThrough } from 'stream'
import data from '$content/press.json'

export const prerender = true

export const GET: RequestHandler = async () => {
  console.log('Creating archive')
  const archive = archiver('zip', {
    zlib: { level: 9 }
  })

  archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
      // log warning
      console.warn(err.message)
    } else {
      // throw error
      throw err
    }
  })

  // good practice to catch this error explicitly
  archive.on('error', function (err) {
    throw err
  })

  const pass = new PassThrough()

  const readableStream = new ReadableStream({
    start(controller) {
      pass.on('data', (chunk: Buffer) => {
        controller.enqueue(chunk)
      })

      pass.on('end', () => {
        controller.close()
      })

      pass.on('error', (err: Error) => {
        controller.error(err)
      })
    }
  })

  archive.pipe(pass)

  for (const path of data.images) {
    if (!path) continue
    const relativePath = path.startsWith('/') ? `.${path}` : path
    archive.file(relativePath, { name: basename(path) })
    console.log(`Adding ${relativePath} to archive`)
  }

  await archive.finalize()

  return new Response(readableStream, {
    headers: {
      'Content-Type': 'application/zip'
    }
  })
}
