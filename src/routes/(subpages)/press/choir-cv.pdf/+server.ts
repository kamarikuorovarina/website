import type { RequestHandler } from './$types'
import fs from 'fs/promises'
import data from '$content/press.json'

export const prerender = true

export const GET: RequestHandler = async () => {
  const file = await fs.readFile(`.${data.choir_cv}`)

  return new Response(file, {
    status: 200,
    headers: {
      'Content-Type': 'application/pdf'
    }
  })
}
