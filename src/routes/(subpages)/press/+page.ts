import type { PageLoad } from './$types'
import { fetchData } from '$collections/pages/press'

export const load: PageLoad = async ({ parent, data }) => {
  const press = await fetchData()
  await parent()

  return {
    ...data,
    title: press.title,
  }
}
