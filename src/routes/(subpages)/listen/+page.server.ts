import type { PageServerLoad } from './$types'
import { listen as wave } from '$lib/waves'
import { fetchData } from '$lib/server/listen'

export const load: PageServerLoad = async () => {
  const listen = await fetchData()

  return {
    listen,
    wave
  }
}
