import { fetchData } from '$collections/pages/listen'
import type { PageLoad } from './$types'

export const load: PageLoad = async ({ parent, data }) => {
  const listen = await fetchData()
  await parent()

  return {
    ...data,
    title: listen.title
  }
}
