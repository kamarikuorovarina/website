import type { PageServerLoad } from './$types'
import { pastEvents as wave } from '$lib/waves'
import { fetchData } from '$collections/pages/past-events'

export const load: PageServerLoad = async () => {
  const pastEvents = await fetchData()

  return {
    wave,
    pastEvents,
    title: pastEvents.title,
    description: pastEvents.summary,
    image: pastEvents.ogImage
  }
}
