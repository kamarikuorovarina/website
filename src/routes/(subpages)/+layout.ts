import { fetchLayoutData } from '$lib/cms/layout-data'
import type { LayoutLoad } from './$types'

export const load: LayoutLoad = async ({ params: { locale } }) => {
  return await fetchLayoutData(locale)
}
