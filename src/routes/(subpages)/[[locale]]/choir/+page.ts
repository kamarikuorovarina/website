import type { PageLoad } from './$types'
import { choir as wave } from '$lib/waves'
import { fetchData } from '$collections/translated-pages/choir'

export const load: PageLoad = async ({ params: { locale } }) => {
  const choir = await fetchData(locale)
  return {
    choir,
    wave,
    title: choir.title,
    description: choir.summary,
    image: choir.ogImage
  }
}
