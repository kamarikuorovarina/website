import { rauhaa as wave } from '$lib/waves'
import { fetchData } from '$collections/pages/rauhaa'
import type { PageLoad } from './$types'

export const load: PageLoad = async () => {
  const rauhaa = await fetchData()
  return {
    rauhaa,
    wave,
    title: rauhaa.title,
    description: rauhaa.summary,
    image: rauhaa.ogImage
  }
}
