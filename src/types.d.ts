declare module 'netlify-cms-media-library-uploadcare' {
  import { MediaLibraryExternalLibrary } from '@staticcms/core'
  const UploadcareMediaLibrary: MediaLibraryExternalLibrary
  export default UploadcareMediaLibrary
}

declare module '*&jpg' {
  const value: string
  export default value
}

declare module '*?jpg' {
  const value: string
  export default value
}

declare module '*as=picture' {
  import { Picture } from 'vite-imagetools'
  const picture: Picture
  export default picture
}

type Maybe<T> = T | null | undefined
