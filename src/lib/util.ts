import { browser } from '$app/environment'
import { compact, join, last, map, omit, pipe, uniq } from 'remeda'
import type { Color } from '../../tailwind.config'

export const dropId = <T extends { id: string }>(item: T) => omit(item, ['id'])

export const classes = (...args: (string | undefined | null | false)[]) =>
  pipe(args, compact, join(' '))

export const colorClasses: { bg: Record<Color, string>; text: Record<Color, string> } = {
  bg: {
    sky: 'bg-sky',
    slate: 'bg-slate',
    silver: 'bg-silver',
    mustard: 'bg-mustard',
    cottoncandy: 'bg-cottoncandy'
  },
  text: {
    sky: 'text-sky',
    slate: 'text-slate',
    silver: 'text-silver',
    mustard: 'text-mustard',
    cottoncandy: 'text-cottoncandy'
  }
}

export const base64encode = (data: string) => {
  if (browser) {
    return btoa(data)
  } else {
    return Buffer.from(data).toString('base64')
  }
}

export const base64decode = (data: string) => {
  if (browser) {
    return atob(data)
  } else {
    return Buffer.from(data, 'base64').toString()
  }
}

export const formattedDates = (dates: Date[]) => {
  const allDates = pipe(
    dates,
    map(
      Intl.DateTimeFormat('fi-FI', {
        day: 'numeric',
        month: 'numeric',
        timeZone: 'Europe/Helsinki'
      }).format
    ),
    uniq()
  )

  return pipe([allDates.slice(0, -1).join(', '), last(allDates)], compact, join(' ja '))
}

export const formattedPlaces = (places: string[]) => {
  const allPlaces = uniq(places)
  return pipe([allPlaces.slice(0, -1).join(', '), last(allPlaces)], compact, join(' ja '))
}
