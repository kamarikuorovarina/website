import type { Data } from '$collections/pages/listen'
import { fetchData as fetchRecords } from '$collections/records'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { fetchData as fetchSamples } from '$lib/server/samples'

export const fetchData = async () => {
  const { default: data } = await import('$content/listen.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [samples, records, contacts, socialMedia] = await Promise.all([
    fetchSamples(),
    fetchRecords(),
    fetchContacts(),
    fetchSocialMedia()
  ])

  return { ...data, samples, records, contacts, socialMedia }
}
