import {
  applyTransforms,
  generateTransforms,
  resolveConfigs,
  builtins,
  builtinOutputFormats,
  extractEntries,
  type Source,
  type Picture
} from 'vite-imagetools'
import fs from 'fs/promises'
import sharp from 'sharp'
import type { Data, DecoratedData } from '$collections/samples'
import { dataFetcher } from '$lib/cms/util'

type Format = 'avif' | 'webp' | 'jpg'

export const createPicture = async (name: string, buffer: ArrayBuffer): Promise<Picture> => {
  const img = sharp(buffer)
  const directives = new URLSearchParams({
    w: '100;400;720',
    format: 'avif;webp;jpg',
    aspect: '16:9',
    fit: 'cover'
  })

  const entries = extractEntries(directives)
  const configs = resolveConfigs(entries, builtinOutputFormats)

  const sources: [Format, Source][] = await Promise.all(
    configs.map(async (config): Promise<[Format, Source]> => {
      const { transforms } = generateTransforms(config, builtins, new URLSearchParams())
      const { image, metadata } = await applyTransforms(transforms, img.clone(), true)
      const { width, format } = metadata as { width: number; format: Format }

      const path = `static/thumbnails/${name}_${width}.${format}`

      await fs.writeFile(path, await image.toBuffer())

      return [format, { src: path.slice(6), w: width }]
    })
  )

  return {
    img: { ...sources[sources.length - 1][1], h: 0 },
    sources: sources.reduce(
      (acc, [format, source]) => ({ ...acc, [format]: [...acc[format], source] }),
      { avif: [], webp: [], jpg: [] }
    )
  }
}

export const fetchData = async () => {
  const data = await dataFetcher(import.meta.glob<{ default: Data }>(`$content/samples/*.json`), {
    orderBy: ({ time }) => time,
    order: 'desc'
  })()
  return Promise.all(data.map((sample) => decorateData(sample)))
}

export const decorateData = async (data: Data): Promise<DecoratedData> => {
  const url = new URL(data.link)
  const id = url.searchParams.get('v') ?? 'invalid'
  const thumbnailUrls = {
    '720': `https://i.ytimg.com/vi/${id}/hq720.jpg`,
    fallback: `https://i.ytimg.com/vi/${id}/hqdefault.jpg`
  }

  let response = await fetch(thumbnailUrls['720'])
  if (!response.ok) {
    response = await fetch(thumbnailUrls['fallback'])
  }

  const buffer = await response.arrayBuffer()
  const thumbnail = await createPicture(id, buffer)
  return {
    ...data,
    thumbnail
  }
}
