import fs from 'fs/promises'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import type { Data, DecoratedData } from '$collections/pages/press'

const humanFileSize = async (path: string): Promise<string> => {
  const stat = await fs.stat(`.${path}`)
  return humanizeBytes(stat.size)
}

const humanizeBytes = (bytes: number): string => {
  let size = bytes

  const thresh = 1024
  const units = ['byte', 'kilobyte', 'megabyte', 'gigabyte', 'terabyte', 'petabyte']

  let u = 0

  while (Math.abs(size) >= thresh) {
    size /= thresh
    ++u
    if (u >= units.length - 1) {
      throw new Error('File size is too large')
    }
  }

  return Intl.NumberFormat('fi-FI', {
    style: 'unit',
    maximumFractionDigits: 1,
    unit: units[u]
  }).format(size)
}

const sizeFromUrlPath = async (path: string, fetchFn: typeof fetch) => {
  const response = await fetchFn(path)
  if (!response.ok || response.body === null) {
    throw new Error('Failed to fetch the URL')
  }

  const reader = response.body.getReader()
  let totalBytes = 0

  let result: ReadableStreamReadResult<Uint8Array>

  do {
    result = await reader.read()

    if (result.done) break

    totalBytes += result.value.length
  } while (!result.done)

  return humanizeBytes(totalBytes)
}

const downloadSizes = async (data: Data, fetchFn: typeof fetch) => {
  const [choir_cv, conductor_cv, images_archive] = await Promise.all([
    humanFileSize(data.choir_cv),
    humanFileSize(data.conductor_cv),
    sizeFromUrlPath('/press/images.zip', fetchFn)
  ])

  return { choir_cv, conductor_cv, images_archive }
}

export const decorateData = async (data: Data, fetchFn: typeof fetch): Promise<DecoratedData> => {
  const [contacts, socialMedia] = await Promise.all([fetchContacts(), fetchSocialMedia()])
  return {
    ...data,
    contacts,
    socialMedia,
    download_sizes: await downloadSizes(data, fetchFn)
  }
}

export const fetchData = async (fetchFn: typeof fetch): Promise<DecoratedData> => {
  const { default: data } = await import('$content/press.json')
  return decorateData(data, fetchFn)
}
