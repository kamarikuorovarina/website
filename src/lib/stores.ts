import { flattenEvents, type Data as EventData } from '$collections/events'
import type { Data as NewsData } from '$collections/news'
import { compact, filter, map, maxBy, minBy, pipe } from 'remeda'
import { isFuture } from 'date-fns'

const closestEvent = (events: EventData[]) =>
  pipe(
    events,
    flattenEvents,
    map((event) => ({ ...event, time: new Date(event.time), type: 'event' }) as const),
    filter(({ time }) => isFuture(time)),
    minBy(({ time }) => time.getTime())
  )

const latestNewsItem = (news: NewsData[]) =>
  pipe(
    news,
    filter(({ archived }) => !archived),
    map((newsItem) => ({ ...newsItem, date: new Date(newsItem.date), type: 'news' }) as const),
    maxBy(({ date }) => date.getTime())
  )

export type LatestHeadlines = ReturnType<typeof latestHeadlines>

export const latestHeadlines = ({ events, news }: { events: EventData[]; news: NewsData[] }) =>
  compact([closestEvent(events), latestNewsItem(news)])
