import type CMS from '@staticcms/core'
import { createElement as h, type Attributes } from 'react'
import {
  CalendarDays,
  Camera,
  ChatBubbleBottomCenterText,
  Cog,
  Document,
  GlobeEuropeAfrica,
  Identification,
  Play,
  RocketLaunch,
  SpeakerWave,
  Star,
  Users,
  Newspaper
} from '@steeze-ui/heroicons'
import { forwardRef } from 'react'

interface IconProps {
  src: IconSource
  theme?: string
  size?: string
  [key: string]: unknown
}

export interface IconSource {
  [key: string]: {
    a: Attributes
    path?: Attributes[]
    rect?: Attributes[]
    circle?: Attributes[]
    polyline?: Attributes[]
    polygon?: Attributes[]
    line?: Attributes[]
  }
}

export const Icon = forwardRef<SVGSVGElement, IconProps>(
  ({ src, size = '100%', theme = 'default', ...rest }: IconProps, forwardedRef) => {
    if (src[theme]) {
      return h('svg', {
        ...src[theme].a,
        xmlns: 'http://www.w3.org/2000/svg',
        width: size,
        height: size,
        ...rest,
        ref: forwardedRef,
        children: [
          ...(src[theme]?.path?.map((a, i) => h('path', { ...a, key: `path-${i}` })) ?? []),
          ...(src[theme]?.rect?.map((a, i) => h('rect', { ...a, key: `rect-${i}` })) ?? []),
          ...(src[theme]?.circle?.map((a, i) => h('circle', { ...a, key: `circle-${i}` })) ?? []),
          ...(src[theme]?.polyline?.map((a, i) => h('polyline', { ...a, key: `polyline-${i}` })) ??
            []),
          ...(src[theme]?.polygon?.map((a, i) => h('polygon', { ...a, key: `polygon-${i}` })) ??
            []),
          ...(src[theme]?.line?.map((a, i) => h('line', { ...a, key: `line-${i}` })) ?? [])
        ]
      })
    } else {
      return null
    }
  }
)

export const registerIcons = (cms: typeof CMS) => {
  cms.registerIcon('document', () => h(Icon, { src: Document }))
  cms.registerIcon('globe', () => h(Icon, { src: GlobeEuropeAfrica }))
  cms.registerIcon('cog', () => h(Icon, { src: Cog }))
  cms.registerIcon('address', () => h(Icon, { src: Identification }))
  cms.registerIcon('comment', () => h(Icon, { src: ChatBubbleBottomCenterText }))
  cms.registerIcon('calendar', () => h(Icon, { src: CalendarDays }))
  cms.registerIcon('camera', () => h(Icon, { src: Camera }))
  cms.registerIcon('users', () => h(Icon, { src: Users }))
  cms.registerIcon('play', () => h(Icon, { src: Play }))
  cms.registerIcon('star', () => h(Icon, { src: Star }))
  cms.registerIcon('speaker', () => h(Icon, { src: SpeakerWave }))
  cms.registerIcon('rocket', () => h(Icon, { src: RocketLaunch }))
  cms.registerIcon('newspaper', () => h(Icon, { src: Newspaper }))
}
