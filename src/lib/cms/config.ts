import { dev } from '$app/environment'
import type { Config } from '@staticcms/core'
import type { Fields } from './collections'
import collections from './collections'
import i18n from './i18n'

const config: Config<Fields> = {
  backend: {
    name: 'gitlab',
    repo: 'kamarikuorovarina/website',
    auth_type: 'pkce',
    app_id: '5f4c7a4e392f9c3138a97f205539e4848c1678f12b87a4ee755de299f81c40b6'
  },
  collections,
  local_backend: dev,
  media_folder: 'media',
  public_folder: '/src/media',
  media_library: {
    max_file_size: 10485760
  },
  i18n
}

export default config
