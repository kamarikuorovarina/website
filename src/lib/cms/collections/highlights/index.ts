import { imagePattern } from '$collections/util'
import { dataFetcher, imageFetcher, pictureFetcher } from '$lib/cms/util'
import type {
  FolderCollection,
  StringOrTextField,
  UUIDField,
  MarkdownField,
  FileOrImageField,
  DateTimeField,
  BooleanField
} from '@staticcms/core'
import type { Picture } from 'vite-imagetools'

export interface Data {
  id: string
  date: string
  title: string
  text: string
  image: string
  image_alt: string
  image_credit?: string
  published: boolean
}

export type DecoratedData = Awaited<ReturnType<typeof fetchData>>[number]

const id: UUIDField = {
  label: 'ID',
  name: 'id',
  widget: 'uuid',
  allow_regenerate: false
}

const date: DateTimeField = {
  label: 'Päivämäärä',
  name: 'date',
  widget: 'datetime',
  time_format: false,
  required: true
}

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const text: MarkdownField = {
  label: 'Teksti',
  name: 'text',
  widget: 'markdown',
  required: true
}

const image: FileOrImageField = {
  label: 'Kuva',
  name: 'image',
  widget: 'image',
  required: true,
  pattern: imagePattern
}

const image_alt: StringOrTextField = {
  label: 'Kuvateksti',
  name: 'image_alt',
  widget: 'string',
  required: true
}

const image_credit: StringOrTextField = {
  label: 'Kuvaaja',
  name: 'image_credit',
  widget: 'string',
  required: false
}

const published: BooleanField = {
  label: 'Julkaistu',
  name: 'published',
  widget: 'boolean',
  required: true,
  default: true
}

const fields = [id, date, title, text, image, image_alt, image_credit, published]
export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'highlights',
  label: 'Kohokohdat',
  label_singular: 'Kohokohta',
  folder: 'src/content/highlights',
  identifier_field: 'id',
  summary_fields: ['title', 'date', 'published'],
  sortable_fields: {
    fields: ['date'],
    default: { field: 'date', direction: 'Descending' }
  },
  create: true,
  format: 'json',
  media_folder: '',
  public_folder: '/src/content/highlights',
  fields,
  icon: 'star'
}

export const fetchData = async () => {
  const data = await dataFetcher(
    import.meta.glob<{ default: Data }>(`$content/highlights/*.json`),
    {
      orderBy: ({ date }) => new Date(date).getTime(),
      order: 'asc',
      filter: ({ published }) => published
    }
  )()

  return await Promise.all(data.map(decorateData))
}

export const decorateData = async (data: Data) => {
  return { ...data, image: getImage(data.image), ogImage: getOGImage(data.image) }
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/highlights/*.jp{,e}g', {
    eager: true,
    query: {
      w: '256;512;900;1800',
      format: 'avif;webp;jpg',
      aspect: '6:4',
      fit: 'cover',
      as: 'picture'
    }
  })
)

export const getOGImage = imageFetcher(
  import.meta.glob<{ default: string }>('$content/highlights/*.jp{,e}g', {
    eager: true,
    query: {
      w: '1200',
      h: '630',
      fit: 'cover',
      format: 'jpg'
    }
  })
)

export default collection
