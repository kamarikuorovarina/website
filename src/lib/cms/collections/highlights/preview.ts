import { useMemo, createElement as h, type FC, useState, useEffect } from 'react'
import { useMediaAsset, type TemplatePreviewProps } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import { decorateData, type Data, type DecoratedData, type Fields } from '.'
import concerts from '.'
import Preview from '$components/HighlightPreview.svelte'
import { dummyPicture } from '$lib/cms/util'

const placeholderImage = 'https://via.placeholder.com/300x300'

const EMPTY: DecoratedData = {
  id: '',
  date: '',
  title: '',
  text: '',
  image: dummyPicture(''),
  image_alt: '',
  ogImage: '',
  published: false
}

const ReactPreview: FC<TemplatePreviewProps<Data, Fields>> = ({ entry, fields, collection }) => {
  const image = useMediaAsset(entry.data?.image, collection, fields, entry) ?? placeholderImage
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo(
    (): { data: DecoratedData } => ({
      data: { ...decoratedData, image: dummyPicture(image) }
    }),
    [decoratedData, image]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(concerts.name, ReactPreview)
}
