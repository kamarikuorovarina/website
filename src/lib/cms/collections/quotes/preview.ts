import { useMemo, createElement as h, type FC } from 'react'
import type { TemplatePreviewProps } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { type Data } from '.'
import Preview from '$components/Quote.svelte'

const ReactPreview: FC<TemplatePreviewProps<Data>> = ({ entry }) => {
  const props = useMemo(
    () => ({
      class: 'w-100 m-8',
      context: 'home' as const,
      data: {
        id: '',
        text: '',
        ...entry.data
      }
    }),
    [entry.data]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app', class: 'bg-white p-8' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
