import { dataFetcher } from '$lib/cms/util'
import type {
  FolderCollection,
  StringOrTextField,
  DateTimeField,
  UUIDField,
  BooleanField
} from '@staticcms/core'

export interface Data {
  id: string
  text: string
  date?: string
  author?: string
  context?: string
  link?: string
  show_on_homepage?: boolean
}

const id: UUIDField = {
  label: 'ID',
  name: 'id',
  widget: 'uuid',
  allow_regenerate: false
}

const text: StringOrTextField = {
  label: 'Teksti',
  name: 'text',
  widget: 'text',
  required: true
}

const date: DateTimeField = {
  label: 'Päivämäärä',
  name: 'date',
  widget: 'datetime',
  date_format: 'dd.MM.yyyy',
  time_format: false,
  required: false,
  default: '',
  format: 'yyyy-MM-dd'
}

const author: StringOrTextField = {
  label: 'Kirjoittaja',
  name: 'author',
  widget: 'string',
  required: false
}

const context: StringOrTextField = {
  label: 'Konteksti',
  name: 'context',
  widget: 'string',
  required: false
}

const link: StringOrTextField = {
  label: 'Linkki',
  name: 'link',
  widget: 'string',
  pattern: ['^https?://', 'Link must start with http:// or https://'],
  required: false
}

const showOnHomepage: BooleanField = {
  label: 'Etusivulla',
  name: 'show_on_homepage',
  widget: 'boolean',
  required: true,
  default: false
}

const fields = [id, text, date, author, context, link, showOnHomepage]

export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'quotes',
  label: 'Lainaukset',
  label_singular: 'Lainaus',
  folder: 'src/content/quotes',
  identifier_field: 'id',
  summary_fields: ['show_on_homepage', 'text'],
  view_groups: [{ label: 'Etusivulla', field: 'show_on_homepage' }],
  create: true,
  format: 'json',
  fields,
  icon: 'comment'
}

export const fetchData = dataFetcher(import.meta.glob<{ default: Data }>('$content/quotes/*.json'))

export default collection
