import { useMemo, createElement as h, type FC, useState, useEffect } from 'react'
import type { TemplatePreviewProps } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import { decorateData, type Data, type DecoratedData, type Fields } from '.'
import concerts from '.'
import Preview from '$components/SamplePreview.svelte'

const EMPTY: DecoratedData = {
  id: '',
  title: '',
  composer: '',
  type: '',
  context: '',
  location: '',
  time: '',
  link: '',
  thumbnail: {
    img: { src: '', w: 0, h: 0 },
    sources: {}
  }
}

const ReactPreview: FC<TemplatePreviewProps<Data, Fields>> = ({ entry }) => {
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo((): { data: DecoratedData } => ({ data: decoratedData }), [decoratedData])

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(concerts.name, ReactPreview)
}
