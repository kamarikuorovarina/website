import { dataFetcher } from '$lib/cms/util'
import type { FolderCollection, StringOrTextField, UUIDField } from '@staticcms/core'

export interface Data {
  id: string
  title: string
  composer: string
  type: string
  context?: string
  location?: string
  time: string
  link: string
}

export type DecoratedData = Awaited<ReturnType<typeof fetchData>>[number]

const id: UUIDField = { name: 'id', label: 'ID', widget: 'uuid', allow_regenerate: false }
const title: StringOrTextField = { name: 'title', label: 'Nimi', widget: 'string', required: true }
const composer: StringOrTextField = {
  name: 'composer',
  label: 'Säveltäjä',
  widget: 'string',
  required: true
}
const type: StringOrTextField = { name: 'type', label: 'Tyyppi', widget: 'string', required: true }
const context: StringOrTextField = {
  name: 'context',
  label: 'Konteksti',
  widget: 'string',
  required: false
}
const location: StringOrTextField = {
  name: 'location',
  label: 'Paikka',
  widget: 'string',
  required: false
}
const time: StringOrTextField = { name: 'time', label: 'Aika', widget: 'string', required: true }
const link: StringOrTextField = {
  name: 'link',
  label: 'YouTube linkki',
  widget: 'string',
  pattern: ['https://www\\.youtube\\.com/watch\\?v=.*', 'Linkin tulee olla YouTube linkki.'],
  required: true
}

const fields = [id, title, composer, type, context, location, time, link]
export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'samples',
  label: 'Ääninäytteet',
  label_singular: 'Ääninäyte',
  folder: 'src/content/samples',
  identifier_field: 'id',
  summary_fields: ['title', 'composer'],
  create: true,
  format: 'json',
  fields,
  icon: 'speaker'
}

export const fetchData = async () => {
  const data = await dataFetcher(import.meta.glob<{ default: Data }>(`$content/samples/*.json`), {
    orderBy: ({ time }) => time
  })()
  return Promise.all([...data].reverse().map((sample) => decorateData(sample)))
}

export const decorateData = async (data: Data) => {
  const url = new URL(data.link)
  const id = url.searchParams.get('v') ?? 'invalid'
  const thumbnailUrl = `https://i.ytimg.com/vi/${id}/hqdefault.jpg`
  const picture = { img: { src: thumbnailUrl, w: 720, h: 0 }, sources: {} }

  return {
    ...data,
    thumbnail: picture
  }
}

export default collection
