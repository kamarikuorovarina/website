import { useMemo, createElement as h, type FC } from 'react'
import type { TemplatePreviewProps } from '@staticcms/core'
import Preview from '$components/Contact.svelte'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { type Data } from '.'

const EMPTY: Data = {
  title: '',
  position: 0
}

const ReactPreview: FC<TemplatePreviewProps<Data>> = ({ entry }) => {
  const props = useMemo(
    () => ({
      data: { ...EMPTY, ...entry.data },
      class: 'w-72 p-4 bg-white',
      style: 'press' as const
    }),
    [entry.data]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
