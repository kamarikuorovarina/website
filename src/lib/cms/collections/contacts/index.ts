import { locales, type Locale } from '$lib/cms/i18n'
import { dataFetcher, i18nDataFetcher, sanitizeLocale } from '$lib/cms/util'
import type { EmailField } from '$lib/cms/widgets/email'
import type { FolderCollection, NumberField, StringOrTextField } from '@staticcms/core'
import { map, pipe, sortBy, values } from 'remeda'
import merge from 'ts-deepmerge'

export interface Data {
  position: number
  title: string
  name?: string
  email?: string
  address?: string
}

const position: NumberField = {
  label: 'Sijoitus',
  name: 'position',
  widget: 'number',
  required: true
}

const title: StringOrTextField = {
  label: 'Nimitys',
  name: 'title',
  widget: 'string',
  required: true,
  i18n: 'translate'
}

const name: StringOrTextField = {
  label: 'Nimi',
  name: 'name',
  widget: 'string',
  required: false
}

const email: EmailField = {
  label: 'Sähköposti',
  name: 'email',
  widget: 'email',
  required: false
}

const address: StringOrTextField = {
  label: 'Osoite',
  name: 'address',
  widget: 'text',
  required: false
}

const fields = [position, title, name, email, address]

export type Fields = (typeof fields)[number]

const contacts: FolderCollection<Fields> = {
  name: 'contacts',
  label: 'Yhteystiedot',
  label_singular: 'Yhteystieto',
  folder: 'src/content/contacts',
  identifier_field: 'id',
  summary_fields: ['title'],
  create: true,
  delete: true,
  format: 'json',
  sortable_fields: {
    fields: ['position'],
    default: {
      field: 'position',
      direction: 'Ascending'
    }
  },
  slug: '{{position}}-{{slug}}',
  fields,
  i18n: {
    structure: 'single_file',
    defaultLocale: 'fi',
    locales: locales
  },
  icon: 'address'
}

export const fetchData = async (unsanitizedLocale?: string) => {
  const locale = sanitizeLocale(unsanitizedLocale)
  const i18nData = import.meta.glob<{ default: Record<Locale, Data> }>('$content/contacts/*.json', {
    eager: true
  })
  return pipe(
    i18nData,
    values,
    map(({ default: data }) => merge(data['fi'], data[locale] ?? {})),
    sortBy(({ position }) => position)
  )
}

export default contacts
