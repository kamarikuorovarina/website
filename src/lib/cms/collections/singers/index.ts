import { dataFetcher } from '$lib/cms/util'
import type { FolderCollection, StringOrTextField, SelectField } from '@staticcms/core'

export interface Data {
  name: string
  voice: 'soprano' | 'alto' | 'tenor' | 'bass'
}

const name: StringOrTextField = {
  label: 'Nimi',
  name: 'name',
  widget: 'string',
  required: true
}

const voice: SelectField = {
  label: 'Ääniala',
  name: 'voice',
  widget: 'select',
  required: true,
  options: [
    { label: 'Sopraano', value: 'soprano' },
    { label: 'Alto', value: 'alto' },
    { label: 'Tenori', value: 'tenor' },
    { label: 'Basso', value: 'bass' }
  ]
}

const fields = [name, voice]

export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'singers',
  label: 'Laulajat',
  label_singular: 'Laulaja',
  folder: 'src/content/singers',
  identifier_field: 'name',
  summary_fields: ['name', 'voice'],
  sortable_fields: {
    fields: ['name', 'voice'],
    default: {
      field: 'name',
      direction: 'Ascending'
    }
  },
  view_groups: [
    {
      label: 'Ääniala',
      field: 'voice'
    }
  ],
  delete: true,
  create: true,
  editor: {
    preview: false
  },
  format: 'json',
  i18n: false,
  fields,
  icon: 'users'
}

export const fetchData = dataFetcher(import.meta.glob<{ default: Data }>('$content/singers/*.json'))

export default collection
