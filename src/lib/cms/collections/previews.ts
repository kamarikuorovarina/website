import type CMS from '@staticcms/core'
import { registerPreview as registerChoirPreview } from './translated-pages/choir/preview'
import { registerPreview as registerHomePreview } from './pages/home/preview'
import { registerPreview as registerConcertsPreview } from './pages/concerts/preview'
import { registerPreview as registerRauhaaPreview } from './pages/rauhaa/preview'
import { registerPreview as registerPressPreview } from './pages/press/preview'
import { registerPreview as registerQuotesPreview } from './quotes/preview'
import { registerPreview as registerEventPreview } from './events/preview'
import { registerPreview as registerContactsPreview } from './contacts/preview'
import { registerPreview as registerListenPreview } from './pages/listen/preview'
import { registerPreview as registerPastEventsPreview } from './pages/past-events/preview'
import { registerPreview as registerRecordsPreview } from './records/preview'
import { registerPreview as registerHighlightsPreview } from './highlights/preview'
import { registerPreview as registerSamplesPreview } from './samples/preview'
import { registerPreview as registerNewsPreview } from './pages/news/preview'
import { registerPreview as registerNewsItemsPreview } from './news/preview'

export const registerPreviews = async (cms: typeof CMS) => {
  registerHomePreview(cms)
  registerChoirPreview(cms)
  registerConcertsPreview(cms)
  registerRauhaaPreview(cms)
  registerQuotesPreview(cms)
  registerEventPreview(cms)
  registerContactsPreview(cms)
  registerPressPreview(cms)
  registerListenPreview(cms)
  registerPastEventsPreview(cms)
  registerRecordsPreview(cms)
  registerHighlightsPreview(cms)
  registerSamplesPreview(cms)
  registerNewsPreview(cms)
  registerNewsItemsPreview(cms)
}
