import { createElement as h, useEffect, useMemo, useState } from 'react'
import type CMS from '@staticcms/core'
import Preview from '$components/ChoirPreview.svelte'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { decorateData, type Data, type Fields, type DecoratedData } from '.'
import { useMediaAsset, type TemplatePreviewComponent } from '@staticcms/core'

const EMPTY: DecoratedData = {
  title: '',
  summary: '',
  introduction: '',
  ogImage: '',
  conductor: { title: '', bio: '', image: '' },
  join: {
    title: '',
    contact_text: '',
    contact_link: { email: '', text: '' },
    shop_text: '',
    shop_link: { url: '', text: '' }
  },
  singers: {
    title: '',
    voices: { soprano: '', alto: '', bass: '', tenor: '' },
    soprano: [],
    alto: [],
    tenor: [],
    bass: []
  },
  contacts: [],
  images: [],
  socialMedia: {
    facebook: '',
    instagram: ''
  }
}

const ReactPreview: TemplatePreviewComponent<Data, Fields> = ({ entry, collection, fields }) => {
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)
  const conductorImage = useMediaAsset(entry.data?.conductor.image, collection, fields, entry)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData({
      ...entry.data,
      conductor: { ...entry.data.conductor, image: conductorImage }
    }).then(setDecoratedData)
  }, [entry.data, conductorImage])

  const props = useMemo((): { data: DecoratedData } => ({ data: decoratedData }), [decoratedData])

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
