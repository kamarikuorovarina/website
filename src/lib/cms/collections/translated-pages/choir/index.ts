import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { fetchData as fetchImages } from '$collections/choir-images'
import { fetchData as fetchSingers, type Data as Singer } from '$collections/singers'
import type { Locale } from '$lib/cms/i18n'
import type {
  BaseField,
  CollectionFile,
  MarkdownField,
  ObjectField,
  StringOrTextField
} from '@staticcms/core'
import { compact, createPipe, filter, map, sortBy } from 'remeda'
import merge from 'ts-deepmerge'
import type { Picture } from 'vite-imagetools'
import { pictureFetcher, sanitizeLocale } from '$lib/cms/util'
import type { EmailField } from '$lib/cms/widgets/email'
import { imagePattern } from '$collections/util'

export type Data = (typeof import('$content/choir.json'))['fi']
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'text',
  required: true,
  i18n: 'translate'
}

const summary: StringOrTextField = {
  label: 'Yhteenveto',
  name: 'summary',
  widget: 'text',
  required: true,
  i18n: 'translate'
}

const introduction: MarkdownField = {
  label: 'Esittely',
  name: 'introduction',
  widget: 'markdown',
  required: true,
  i18n: 'translate',
  toolbar_buttons: {}
}

const conductor: ObjectField = {
  i18n: true,
  label: 'Johtaja',
  name: 'conductor',
  required: true,
  widget: 'object',
  fields: [
    {
      label: 'Nimike',
      name: 'title',
      widget: 'string',
      required: true,
      i18n: 'translate'
    },
    {
      label: 'Esittely',
      name: 'bio',
      widget: 'markdown',
      required: true,
      i18n: 'translate',
      toolbar_buttons: {}
    },
    {
      label: 'Kuva',
      name: 'image',
      widget: 'image',
      required: true,
      i18n: 'duplicate',
      pattern: imagePattern
    }
  ]
}

const join: ObjectField<BaseField | EmailField> = {
  i18n: true,
  label: 'Liity jäseneksi',
  name: 'join',
  required: true,
  widget: 'object',
  fields: [
    {
      label: 'Otsikko',
      name: 'title',
      widget: 'string',
      required: true,
      i18n: 'translate'
    },
    {
      label: 'Teksti yhteydenotto',
      name: 'contact_text',
      widget: 'markdown',
      required: true,
      i18n: 'translate'
    },
    {
      label: 'Linkki yhteydenotto',
      name: 'contact_link',
      widget: 'object',
      i18n: true,
      fields: [
        { label: 'Teksti', name: 'text', widget: 'string', required: true, i18n: 'translate' },
        { label: 'Maili', name: 'email', widget: 'email', required: true, i18n: 'duplicate' }
      ]
    },
    {
      label: 'Teksti verkkokauppa',
      name: 'shop_text',
      widget: 'markdown',
      required: true,
      i18n: 'translate'
    },
    {
      label: 'Linkki verkkokauppa',
      name: 'shop_link',
      widget: 'object',
      i18n: true,
      fields: [
        { label: 'Teksti', name: 'text', widget: 'string', required: true, i18n: 'translate' },
        { label: 'URL', name: 'url', widget: 'string', required: true, i18n: 'duplicate' }
      ]
    }
  ]
}

const singers: ObjectField = {
  i18n: true,
  label: 'Laulajat',
  name: 'singers',
  required: true,
  widget: 'object',
  fields: [
    {
      label: 'Otsikko',
      name: 'title',
      widget: 'string',
      required: true,
      i18n: 'translate'
    },
    {
      label: 'Äänet',
      name: 'voices',
      widget: 'object',
      required: true,
      i18n: true,
      fields: [
        {
          label: 'Sopraanot',
          name: 'soprano',
          widget: 'string',
          required: true,
          i18n: 'translate'
        },
        {
          label: 'Altot',
          name: 'alto',
          widget: 'string',
          required: true,
          i18n: 'translate'
        },
        {
          label: 'Tenorit',
          name: 'tenor',
          widget: 'string',
          required: true,
          i18n: 'translate'
        },
        {
          label: 'Bassot',
          name: 'bass',
          widget: 'string',
          required: true,
          i18n: 'translate'
        }
      ]
    }
  ]
}

const fields = [title, summary, introduction, conductor, join, singers]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'choir',
  label: 'Kuoro',
  file: 'src/content/choir.json',
  media_folder: 'choir/media',
  public_folder: '/src/content/choir/media',
  i18n: true,
  fields
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/choir/media/*.jp{,e}g', {
    eager: true,
    query: {
      w: '220;440;600',
      format: 'avif;webp;jpg',
      as: 'picture'
    }
  })
)

export const fetchData = async (locale?: string) => {
  const { default: i18nData } = await import('$content/choir.json')
  const data = merge(i18nData['fi'], i18nData[(locale as Locale) ?? 'fi'])

  return decorateData(data)
}

const sortSingers = (voice: 'soprano' | 'alto' | 'tenor' | 'bass') =>
  createPipe(
    filter((singer: Singer) => singer.voice === voice),
    map((singer) => singer.name),
    sortBy((name) => name.split(/\s+/).reverse().join(' '))
  )

export const decorateData = async (data: Data) => {
  const [contacts, singers, images, socialMedia] = await Promise.all([
    fetchContacts(),
    fetchSingers(),
    fetchImages(),
    fetchSocialMedia()
  ])

  return {
    ...data,
    ogImage: images[0].ogImage,
    singers: {
      ...data.singers,
      soprano: sortSingers('soprano')(singers),
      alto: sortSingers('alto')(singers),
      tenor: sortSingers('tenor')(singers),
      bass: sortSingers('bass')(singers)
    },
    contacts,
    images,
    socialMedia
  }
}

export const fetchLink = async (locale?: string) => {
  const { default: i18nData } = await import('$content/choir.json')
  const { title } = i18nData[sanitizeLocale(locale)]

  return { title, href: `/${compact([locale, 'choir']).join('/')}` }
}

export default collection
