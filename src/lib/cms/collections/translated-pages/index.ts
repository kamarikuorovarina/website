import type { FilesCollection } from '@staticcms/core'
import choir from './choir'

const files = [choir]
export type Fields = (typeof files)[number]['fields'][number]

const pages: FilesCollection<Fields> = {
  name: 'translated-pages',
  label: 'Monikieliset Sivut',
  i18n: {
    structure: 'single_file',
    locales: ['fi', 'se', 'en'],
    defaultLocale: 'fi'
  },
  icon: 'globe',
  files
}

export default pages
