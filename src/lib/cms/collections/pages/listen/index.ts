import { fetchData as fetchRecords } from '$collections/records'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { fetchData as fetchSamples } from '$collections/samples'
import type { CollectionFile, MarkdownField, ObjectField, StringOrTextField } from '@staticcms/core'
import type { EmailField } from '$lib/cms/widgets/email'

export type Data = typeof import('$content/listen.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const discount: ObjectField<MarkdownField | EmailField> = {
  label: 'Määräalennus',
  name: 'discount',
  widget: 'object',
  required: true,
  fields: [
    { name: 'text', label: 'Teksti', widget: 'markdown', required: true },
    { name: 'contact', label: 'Mailosoite', widget: 'email', required: true }
  ]
}

const shipping: ObjectField<MarkdownField | EmailField> = {
  label: 'Toimituskulut',
  name: 'shipping',
  widget: 'object',
  required: true,
  fields: [
    { name: 'text', label: 'Teksti', widget: 'markdown', required: true },
    { name: 'contact', label: 'Mailiosoite', widget: 'email', required: true }
  ]
}

const fields = [title, discount, shipping]
export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'listen',
  label: 'Kuuntele',
  file: 'src/content/listen.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/listen.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [samples, records, contacts, socialMedia] = await Promise.all([
    fetchSamples(),
    fetchRecords(),
    fetchContacts(),
    fetchSocialMedia()
  ])

  return { ...data, samples, records, contacts, socialMedia }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/listen.json')
  return { title, href: '/listen' }
}

export default collection
