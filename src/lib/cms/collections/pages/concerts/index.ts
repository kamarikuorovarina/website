import { flattenEvents } from '$collections/events'
import type { CollectionFile, StringOrTextField } from '@staticcms/core'
import { fetchData as fetchEvents } from '$collections/events'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { filter, map, pipe, sortBy } from 'remeda'
import { isFuture, isToday } from 'date-fns'

export type Data = typeof import('$content/concerts.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'text',
  required: true
}

const summary: StringOrTextField = {
  label: 'Yhteenveto',
  name: 'summary',
  widget: 'text',
  required: false
}

const fields = [title, summary]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'concerts',
  label: 'Konsertit',
  file: 'src/content/concerts.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/concerts.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [events, contacts, socialMedia] = await Promise.all([
    fetchEvents(true),
    fetchContacts(),
    fetchSocialMedia()
  ])

  return {
    ...data,
    events: pipe(
      events,
      flattenEvents,
      map((event) => ({ ...event, time: new Date(event.time) })),
      sortBy(({ time }) => time.getTime()),
      filter(({ time }) => isFuture(time) || isToday(time)),
      map((event) => ({ ...event, time: event.time.toISOString() }))
    ),
    contacts,
    socialMedia
  }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/concerts.json')
  return { title, href: '/concerts' }
}

export default collection
