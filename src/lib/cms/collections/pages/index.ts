import type { FilesCollection } from '@staticcms/core'
import home from './home'
import news from './news'
import concerts from './concerts'
import rauhaa from './rauhaa'
import press from './press'
import listen from './listen'
import pastEvents from './past-events'

const files = [home, news, concerts, rauhaa, press, listen, pastEvents]
export type Fields = (typeof files)[number]['fields'][number]

const pages: FilesCollection<Fields> = {
  name: 'pages',
  label: 'Sivut',
  i18n: false,
  icon: 'document',
  files
}

export default pages
