import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchNewsItems } from '$collections/news'
import { fetchData as fetchQuotes } from '$collections/quotes'
import { fetchData as fetchHighlights } from '$collections/highlights'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import type { CollectionFile, StringOrTextField } from '@staticcms/core'
import { filter, pipe, sortBy } from 'remeda'

export type Data = typeof import('$content/news.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>
export type DecoratedNewsItem = DecoratedData['newsItems'][number]

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const fields = [title]
export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'news',
  label: 'Ajankohtaista',
  file: 'src/content/news.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/news.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [contacts, allNewsItems, quotes, socialMedia, highlights] = await Promise.all([
    fetchContacts(),
    fetchNewsItems(true),
    fetchQuotes(),
    fetchSocialMedia(),
    fetchHighlights()
  ])

  const newsItems = pipe(
    allNewsItems,
    filter(({ archived }) => !archived),
    sortBy(({ date }) => -new Date(date).getTime())
  )

  return {
    ...data,
    contacts,
    newsItems,
    highlights,
    quotes,
    socialMedia,
    ogImage: highlights[highlights.length - 1].ogImage
  }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/news.json')
  return { title, href: '/news' }
}

export default collection
