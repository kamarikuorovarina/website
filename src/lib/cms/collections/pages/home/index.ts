import type { CollectionFile, StringOrTextField } from '@staticcms/core'
import { fetchData as fetchQuotes } from '$collections/quotes'
import { fetchData as fetchEvents } from '$collections/events'
import { fetchData as fetchNews } from '$collections/news'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchChoir } from '$collections/translated-pages/choir'
import { fetchData as fetchConcerts } from '$collections/pages/concerts'
import { fetchData as fetchListen } from '$collections/pages/listen'
import { fetchData as fetchRauhaa } from '$collections/pages/rauhaa'
import { fetchData as fetchPastEvents } from '$collections/pages/past-events'
import { fetchData as fetchPress } from '$collections/pages/press'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'

export interface Data {
  introduction: string
}

export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const introduction: StringOrTextField = {
  label: 'Esittely',
  name: 'introduction',
  widget: 'text',
  required: true
}

const fields = [introduction]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'home',
  label: 'Etusivu',
  file: 'src/content/home.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/home.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [
    contacts,
    quotes,
    events,
    news,
    { title: choir },
    { title: concerts },
    { title: listen },
    { title: rauhaa },
    { title: pastEvents },
    { title: press },
    socialMedia
  ] = await Promise.all([
    fetchContacts(),
    fetchQuotes(),
    fetchEvents(false),
    fetchNews(false),
    fetchChoir(),
    fetchConcerts(),
    fetchListen(),
    fetchRauhaa(),
    fetchPastEvents(),
    fetchPress(),
    fetchSocialMedia()
  ])

  return {
    ...data,
    contacts,
    quotes: quotes.filter(({ show_on_homepage }) => show_on_homepage),
    events,
    news,
    labels: { choir, concerts, listen, rauhaa, pastEvents, press },
    socialMedia
  }
}

export const fetchMetadata = async () => {
  const {
    default: { introduction }
  } = await import('$content/home.json')

  return {
    description: introduction
  }
}

export default collection
