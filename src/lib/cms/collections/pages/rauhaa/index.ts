import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { fetchRauhaa } from '$collections/records'
import { fetchData as fetchListen } from '$collections/pages/listen'
import { imageFetcher, pictureFetcher } from '$lib/cms/util'
import type { EmailField } from '$lib/cms/widgets/email'
import type {
  BaseField,
  CollectionFile,
  MarkdownField,
  ObjectField,
  StringOrTextField,
  FileOrImageField
} from '@staticcms/core'
import type { Picture } from 'vite-imagetools'
import { imagePattern } from '$collections/util'

export type Data = typeof import('$content/rauhaa.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const summary: StringOrTextField = {
  label: 'Yhteenveto',
  name: 'summary',
  widget: 'text',
  required: true,
  i18n: 'translate'
}

const image: FileOrImageField = {
  label: 'Kuva',
  name: 'image',
  widget: 'image',
  required: true,
  pattern: imagePattern
}

const introduction: MarkdownField = {
  label: 'Esittely',
  name: 'introduction',
  widget: 'markdown',
  required: true
}

const concerts: ObjectField = {
  label: 'Konsertit',
  name: 'concerts',
  widget: 'object',
  required: true,

  fields: [
    {
      label: 'Otsikko',
      name: 'title',
      widget: 'string',
      required: true
    },
    {
      label: 'Kuvaus',
      name: 'description',
      widget: 'markdown',
      required: true,
      toolbar_buttons: {}
    },
    {
      label: 'Linkkiteksti',
      name: 'link_text',
      widget: 'string',
      required: true
    }
  ]
}

const business: ObjectField<BaseField | EmailField> = {
  label: 'Yrityksille',
  name: 'business',
  widget: 'object',
  required: true,
  fields: [
    {
      label: 'Kuva',
      name: 'image',
      widget: 'image',
      required: true,
      pattern: imagePattern
    },
    {
      label: 'Kuvateksti',
      name: 'image_alt',
      widget: 'string',
      required: true
    },
    {
      label: 'Kuvaaja',
      name: 'image_credit',
      widget: 'string',
      required: true
    },
    {
      label: 'Otsikko',
      name: 'title',
      widget: 'string',
      required: true
    },
    {
      label: 'Teksti',
      name: 'text',
      widget: 'markdown',
      required: true,
      toolbar_buttons: {}
    },
    {
      label: 'Maili linkki',
      name: 'mail_link',
      widget: 'object',
      required: true,
      fields: [
        {
          label: 'Teksti',
          name: 'text',
          widget: 'string',
          required: true
        },
        {
          label: 'Mailiosoite',
          name: 'email',
          widget: 'email',
          required: true
        }
      ]
    }
  ]
}

const fields = [title, summary, image, introduction, concerts, business]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'rauhaa',
  label: 'Rauhaa',
  file: 'src/content/rauhaa.json',
  media_folder: 'rauhaa/media',
  public_folder: '/src/content/rauhaa/media',
  fields
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/rauhaa/media/*.jp{,e}g', {
    eager: true,
    query: {
      w: '500;1000;1400;2400',
      format: 'avif;webp;jpg',
      as: 'picture'
    }
  })
)

export const getOGImage = imageFetcher(
  import.meta.glob<{ default: string }>('$media/static/menu/*.jp{,e}g', {
    eager: true,
    query: {
      w: '1200',
      h: '630',
      fit: 'cover',
      format: 'jpg'
    }
  })
)

export const fetchData = async () => {
  const { default: data } = await import('$content/rauhaa.json')

  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [contacts, socialMedia, record, { discount, shipping }] = await Promise.all([
    fetchContacts(),
    fetchSocialMedia(),
    fetchRauhaa(),
    fetchListen()
  ])

  const image = getImage(data.image)
  const ogImage = getOGImage('/src/media/static/menu/rauhaa.jpg')
  const businessImage = getImage(data.business.image)

  return {
    ...data,
    image,
    business: {
      ...data.business,
      image: businessImage
    },
    ogImage,
    record,
    discount,
    shipping,
    socialMedia,
    contacts
  }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/rauhaa.json')
  return { title, href: '/rauhaa' }
}

export default collection
