import { createElement as h, useEffect, useMemo, useState } from 'react'
import type CMS from '@staticcms/core'
import Preview from '$components/RauhaaPreview.svelte'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { decorateData, type Data, type Fields, type DecoratedData } from '.'
import { useMediaAsset, type TemplatePreviewComponent } from '@staticcms/core'
import { dummyPicture } from '$lib/cms/util'

const EMPTY: DecoratedData = {
  title: '',
  summary: '',
  image: dummyPicture(''),
  ogImage: '',
  introduction: '',
  discount: {
    text: '',
    contact: ''
  },
  shipping: {
    text: '',
    contact: ''
  },
  concerts: {
    title: '',
    description: '',
    link_text: ''
  },
  record: {
    title: '',
    cover: '',
    price: NaN,
    description: '',
    listen_link: '',
    order_link: '',
    info: {
      title: '',
      data: [],
      songs: []
    }
  },
  business: {
    image: dummyPicture(''),
    image_credit: '',
    title: '',
    text: '',
    image_alt: '',
    mail_link: {
      text: '',
      email: '',
      link: ''
    }
  },
  contacts: [],
  socialMedia: {
    facebook: '',
    instagram: ''
  }
}

const placeholderImage = 'https://via.placeholder.com/300x300'

const ReactPreview: TemplatePreviewComponent<Data, Fields> = ({ entry, fields }) => {
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)
  const image = useMediaAsset(entry.data?.image, collection, fields, entry) ?? placeholderImage
  const mockup =
    useMediaAsset(entry.data?.business.image, collection, fields, entry) ?? placeholderImage

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo(
    (): { data: DecoratedData } => ({
      data: {
        ...decoratedData,
        image: dummyPicture(image),
        business: { ...decoratedData.business, image: dummyPicture(mockup) }
      }
    }),
    [decoratedData, mockup, image]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
