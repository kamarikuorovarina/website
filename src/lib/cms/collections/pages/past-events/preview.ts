import { createElement as h, useEffect, useMemo, useState } from 'react'
import type CMS from '@staticcms/core'
import Preview from '$components/PastEventsPreview.svelte'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { decorateData, type Data, type Fields, type DecoratedData } from '.'
import type { TemplatePreviewComponent } from '@staticcms/core'

const EMPTY: DecoratedData = {
  title: '',
  summary: '',
  ogImage: '',
  events: [],
  highlights: [],
  quotes: [],
  contacts: [],
  socialMedia: {
    facebook: '',
    instagram: ''
  }
}

const ReactPreview: TemplatePreviewComponent<Data, Fields> = ({ entry }) => {
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo(
    (): { data: DecoratedData } => ({
      data: decoratedData
    }),
    [decoratedData]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
