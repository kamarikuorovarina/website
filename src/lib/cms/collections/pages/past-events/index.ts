import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchEvents } from '$collections/events'
import { fetchData as fetchQuotes } from '$collections/quotes'
import { fetchData as fetchHighlights } from '$collections/highlights'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import type { CollectionFile, StringOrTextField } from '@staticcms/core'
import { isPast } from 'date-fns'
import { filter, map, minBy, pipe, sortBy } from 'remeda'

export type Data = typeof import('$content/past-events.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>
export type DecoratedEvent = DecoratedData['events'][number]

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const introduction: StringOrTextField = {
  label: 'Yhteenveto',
  name: 'summary',
  widget: 'text',
  required: true
}

const fields = [title, introduction]
export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'past-events',
  label: 'Kohokohdat',
  file: 'src/content/past-events.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/past-events.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [contacts, allEvents, quotes, socialMedia, highlights] = await Promise.all([
    fetchContacts(),
    fetchEvents(false),
    fetchQuotes(),
    fetchSocialMedia(),
    fetchHighlights()
  ])

  const events = pipe(
    allEvents,
    map((event) => {
      const instances = pipe(
        event.instances,
        map((instance) => ({ ...instance, time: new Date(instance.time) })),
        sortBy(({ time }) => time.getTime())
      )
      const dates = instances.map(({ time }) => time)
      const places = instances.map(({ place }) => place)

      return { ...event, dates, places }
    }),
    filter(({ dates }) => dates.every((date) => isPast(date))),
    sortBy(
      ({ dates }) =>
        -(
          pipe(
            dates,
            minBy((date) => date.getTime())
          ) ?? 0
        )
    )
  )

  return {
    ...data,
    contacts,
    events,
    highlights,
    quotes,
    socialMedia,
    ogImage: highlights[highlights.length - 1].ogImage
  }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/past-events.json')
  return { title, href: '/highlights' }
}

export default collection
