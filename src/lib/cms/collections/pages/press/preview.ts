import { createElement as h, useEffect, useMemo, useState } from 'react'
import type CMS from '@staticcms/core'
import Preview from '$components/PressPreview.svelte'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import collection, { decorateData, type Data, type DecoratedData, type Fields } from '.'
import type { TemplatePreviewComponent } from '@staticcms/core'

const EMPTY: DecoratedData = {
  title: '',
  org_name: '',
  booking: {
    text: '',
    title: '',
    contact_link: {
      text: '',
      email: ''
    }
  },
  conductor_cv: '',
  choir_cv: '',
  images: [],
  contacts: [],
  download_sizes: {
    choir_cv: '0 MB',
    conductor_cv: '0 MB',
    images_archive: '0 MB'
  },
  socialMedia: {
    facebook: '',
    instagram: ''
  }
}

const ReactPreview: TemplatePreviewComponent<Data, Fields> = ({ entry }) => {
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo((): { data: DecoratedData } => ({ data: decoratedData }), [decoratedData])

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(collection.name, ReactPreview)
}
