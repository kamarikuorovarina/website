import type {
  BaseField,
  CollectionFile,
  FileOrImageField,
  ObjectField,
  StringOrTextField
} from '@staticcms/core'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import type { EmailField } from '$lib/cms/widgets/email'
import { imagePattern } from '$collections/util'

export type Data = typeof import('$content/press.json')
export type DecoratedData = Awaited<ReturnType<typeof fetchData>>

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'string',
  required: true
}

const org_name: StringOrTextField = {
  label: 'Yhdistyksen nimi',
  name: 'org_name',
  widget: 'string',
  required: true
}

const booking: ObjectField<BaseField | EmailField> = {
  label: 'Varaukset',
  name: 'booking',
  required: true,
  widget: 'object',
  fields: [
    { label: 'Otsikko', name: 'title', widget: 'string', required: true },
    { label: 'Teksti', name: 'text', widget: 'markdown', required: true },
    {
      label: 'Yhteydenottolinkki',
      name: 'contact_link',
      widget: 'object',
      required: true,
      fields: [
        { label: 'Teksti', name: 'text', widget: 'string', required: true },
        { label: 'Sähköposti', name: 'email', widget: 'email', required: true }
      ]
    }
  ]
}

const choirCV: FileOrImageField = {
  label: 'Kuoron CV',
  name: 'choir_cv',
  media_library: {
    max_file_size: 52428800
  },
  media_folder: 'press/media',
  public_folder: '/src/content/press/media',
  widget: 'file',
  pattern: ['.*\\.pdf$', 'Only PDF files are allowed.']
}

const conductorCV: FileOrImageField = {
  label: 'Kuoronjohtajan CV',
  name: 'conductor_cv',
  media_library: {
    max_file_size: 52428800
  },
  media_folder: 'press/media',
  public_folder: '/src/content/press/media',
  widget: 'file',
  pattern: ['.*\\.pdf$', 'Only PDF files are allowed.']
}

const images: FileOrImageField = {
  label: 'Kuvat',
  name: 'images',
  media_library: {
    max_file_size: 52428800
  },
  media_folder: 'press/media',
  public_folder: '/src/content/press/media',
  multiple: true,
  widget: 'image'
}

const fields = [title, org_name, booking, choirCV, conductorCV, images]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'press',
  label: 'Lehdistö',
  file: 'src/content/press.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/press.json')
  return decorateData(data)
}

export const decorateData = async (data: Data) => {
  const [contacts, socialMedia] = await Promise.all([fetchContacts(), fetchSocialMedia()])
  return {
    ...data,
    contacts,
    socialMedia,
    download_sizes: {
      choir_cv: '2,5 MT',
      conductor_cv: '2,8 MT',
      images_archive: '25 MT'
    }
  }
}

export const fetchLink = async () => {
  const {
    default: { title }
  } = await import('$content/press.json')
  return { title, href: '/press' }
}

export default collection
