import pages from './pages'
import translatedPages from './translated-pages'
import settings from './settings'
import quotes from './quotes'
import events from './events'
import news from './news'
import contacts from './contacts'
import choirImages from './choir-images'
import singers from './singers'
import records from './records'
import highlights from './highlights'
import samples from './samples'

const fileCollections = [pages, translatedPages, settings]
const folderCollections = [
  news,
  contacts,
  quotes,
  events,
  choirImages,
  singers,
  records,
  highlights,
  samples
]

type FileCollections = typeof fileCollections
type FolderCollections = typeof folderCollections

const collections = [...fileCollections, ...folderCollections]

export type Fields =
  | FileCollections[number]['files'][number]['fields'][number]
  | FolderCollections[number]['fields'][number]

export default collections
