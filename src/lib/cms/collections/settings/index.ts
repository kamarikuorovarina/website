import type { FilesCollection } from '@staticcms/core'
import socialMedia from './social-media'

const files = [socialMedia]
export type Fields = (typeof files)[number]['fields'][number]

const pages: FilesCollection<Fields> = {
  name: 'settings',
  label: 'Asetukset',
  i18n: false,
  editor: {
    preview: false
  },
  icon: 'cog',
  files
}

export default pages
