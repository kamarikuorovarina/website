import type { CollectionFile, StringOrTextField } from '@staticcms/core'

export type Data = typeof import('$content/settings/social-media.json')

const facebook: StringOrTextField = {
  label: 'Facebook',
  name: 'facebook',
  widget: 'string',
  required: true
}

const instagram: StringOrTextField = {
  label: 'Instagram',
  name: 'instagram',
  widget: 'string',
  required: true
}

const fields = [facebook, instagram]

export type Fields = (typeof fields)[number]

const collection: CollectionFile<Fields> = {
  name: 'social-media',
  label: 'Some',
  file: 'src/content/settings/social-media.json',
  i18n: false,
  fields
}

export const fetchData = async () => {
  const { default: data } = await import('$content/settings/social-media.json')
  return data
}

export default collection
