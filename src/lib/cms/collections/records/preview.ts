import { useMemo, createElement as h, type FC, useEffect, useState } from 'react'
import { useMediaAsset, type TemplatePreviewProps } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import type { Data, DecoratedData, Fields } from '.'
import concerts, { decorateData } from '.'
import Preview from '$components/RecordPreview.svelte'
import { dummyPicture } from '$lib/cms/util'

const placeholderImage = 'https://via.placeholder.com/300x300'

const EMPTY: DecoratedData = {
  title: '',
  cover: dummyPicture(''),
  description: '',
  info: {
    title: '',
    data: [],
    songs: []
  },
  listen_link: '',
  order_link: '',
  price: NaN
}

const ReactPreview: FC<TemplatePreviewProps<Data, Fields>> = ({ entry, fields, collection }) => {
  const cover = useMediaAsset(entry.data?.cover, collection, fields, entry) ?? placeholderImage
  const [decoratedData, setDecoratedData] = useState<DecoratedData>(EMPTY)

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData(entry.data).then(setDecoratedData)
  }, [entry.data])

  const props = useMemo(
    (): { data: DecoratedData } => ({
      data: { ...decoratedData, cover: dummyPicture(cover) }
    }),
    [decoratedData, cover]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(concerts.name, ReactPreview)
}
