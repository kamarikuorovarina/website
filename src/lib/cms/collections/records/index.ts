import { imagePattern } from '$collections/util'
import { dataFetcher, pictureFetcher } from '$lib/cms/util'

import type {
  FolderCollection,
  StringOrTextField,
  FileOrImageField,
  ObjectField,
  NumberField
} from '@staticcms/core'
import type { Picture } from 'vite-imagetools'

interface DataField {
  label: string
  value: string
}

export interface Song {
  name: string
  composer: string
}

export interface Data {
  title: string
  cover: string
  description: string
  listen_link: string
  order_link: string
  price: number
  info: {
    title: string
    data: DataField[]
    songs: Song[]
  }
}

export type DecoratedData = Awaited<ReturnType<typeof fetchData>>[number]

const title: StringOrTextField = {
  label: 'Levyn nimi',
  name: 'title',
  widget: 'string',
  required: true
}

const cover: FileOrImageField = {
  label: 'Kansikuva',
  name: 'cover',
  widget: 'image',
  required: true,
  pattern: imagePattern
}

const description: StringOrTextField = {
  label: 'Kuvaus',
  name: 'description',
  widget: 'string',
  required: true
}

const listen_link: StringOrTextField = {
  label: 'Kuuntele linkki',
  name: 'listen_link',
  widget: 'string',
  required: true
}

const order_link: StringOrTextField = {
  label: 'Kuuntele linkki',
  name: 'order_link',
  widget: 'string',
  required: true
}

const price: NumberField = {
  label: 'Hinta',
  name: 'price',
  widget: 'number',
  required: true
}

const info: ObjectField = {
  label: 'Info',
  name: 'info',
  widget: 'object',
  required: true,
  fields: [
    {
      label: 'Otiskko',
      name: 'title',
      widget: 'string',
      required: true
    },
    {
      label: 'Tiedot',
      label_singular: 'Tieto',
      name: 'data',
      widget: 'list',
      required: true,
      fields: [
        {
          label: 'Käsite',
          name: 'label',
          widget: 'string',
          required: true
        },
        {
          label: 'Arvo',
          name: 'value',
          widget: 'string',
          required: true
        }
      ]
    },
    {
      label: 'Kappaleet',
      label_singular: 'Kappale',
      name: 'songs',
      widget: 'list',
      required: true,
      fields: [
        {
          label: 'Nimi',
          name: 'name',
          widget: 'string',
          required: true
        },
        {
          label: 'Säveltäjä',
          name: 'composer',
          widget: 'text',
          required: true
        }
      ]
    }
  ]
}
const fields = [title, cover, description, price, listen_link, order_link, info]
export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'records',
  label: 'Levyt',
  label_singular: 'Levy',
  folder: 'src/content/records',
  identifier_field: 'title',
  summary_fields: ['title'],
  create: true,
  format: 'json',
  media_folder: '',
  public_folder: '/src/content/records',
  fields,
  icon: 'play'
}

export const fetchData = async () => {
  const data = await dataFetcher(import.meta.glob<{ default: Data }>(`$content/records/*.json`))()
  return Promise.all(data.map(decorateData))
}

export const decorateData = async (data: Data) => {
  const cover = getImage(data.cover)
  return { ...data, cover }
}

export const fetchRauhaa = () =>
  import(`$content/records/rauhaa.json`).then(({ default: data }) => data)

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/records/*.jp{,e}g', {
    eager: true,
    query: {
      w: '256;512;1024;1400;2400',
      format: 'avif;webp;jpg',
      as: 'picture'
    }
  })
)

export default collection
