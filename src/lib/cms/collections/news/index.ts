import type { Song } from '$collections/records'
import { imagePattern } from '$collections/util'
import { dataFetcher, imageFetcher, pictureFetcher } from '$lib/cms/util'
import { type Data as Event, fetchData as fetchEvents } from '$collections/events'

import type {
  FolderCollection,
  StringOrTextField,
  DateTimeField,
  UUIDField,
  MarkdownField,
  ListField,
  PreSaveEventListener,
  HiddenField,
  RelationField,
  BooleanField
} from '@staticcms/core'
import type { Picture } from 'vite-imagetools'

export interface Data {
  id: string
  slug: string
  title: string
  date: string
  images: { image: string; caption: string }[]
  excerpt: string
  content: string
  program: Song[]
  event?: string
  archived?: boolean
}

export interface DecoratedData extends Omit<Data, 'images' | 'event'> {
  event?: Event
  images: { image: Picture; caption: string }[]
  ogImage?: string
}

const id: UUIDField = {
  label: 'ID',
  name: 'id',
  widget: 'uuid',
  allow_regenerate: false
}

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'text',
  required: true
}

const date: DateTimeField = {
  label: 'Päivämäärä',
  name: 'date',
  widget: 'datetime',
  time_format: false
}

const year: HiddenField = {
  label: 'Vuosi',
  name: 'year',
  widget: 'hidden',
  required: false
}

const images: ListField = {
  label: 'Kuva(t)',
  name: 'images',
  widget: 'list',
  required: false,
  fields: [
    { name: 'image', label: 'Kuva', widget: 'image', required: true, pattern: imagePattern },
    { name: 'caption', label: 'Kuvateksti', widget: 'string', required: false, default: '' }
  ]
}

const excerpt: StringOrTextField = {
  label: 'Yhteenveto',
  name: 'excerpt',
  widget: 'text',
  required: true
}

const content: MarkdownField = {
  label: 'Sisältö',
  name: 'content',
  widget: 'markdown',
  required: true
}

const program: ListField = {
  label: 'Ohjelma',
  label_singular: 'Kappale',
  name: 'program',
  widget: 'list',
  required: false,
  fields: [
    {
      label: 'Nimi',
      name: 'name',
      widget: 'string',
      required: true
    },
    {
      label: 'Säveltäjä',
      name: 'composer',
      widget: 'text',
      required: true
    }
  ]
}

const event: RelationField = {
  label: 'Tapahtuma',
  name: 'event',
  widget: 'relation',
  collection: 'events',
  search_fields: ['title'],
  value_field: 'id',
  display_fields: ['title'],
  required: false
}

const archived: BooleanField = {
  label: 'Arkistoitu',
  name: 'archived',
  widget: 'boolean',
  required: true,
  default: false
}

const fields = [id, title, date, year, images, excerpt, content, program, event, archived]
export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'news_items',
  label: 'Ajankohtaista',
  label_singular: 'Uutinen',
  folder: 'src/content/news',
  identifier_field: 'id',
  summary_fields: ['date', 'title'],
  sortable_fields: {
    fields: ['date'],
    default: { field: 'date', direction: 'Descending' }
  },
  view_groups: [
    {
      label: 'Vuonna',
      field: 'year'
    }
  ],
  slug: '{{year}}-{{title}}',
  create: true,
  format: 'json',
  media_folder: '',
  public_folder: '/src/content/news',
  fields,
  icon: 'newspaper'
}

export async function fetchData<Decorate extends boolean>(
  decorate: Decorate
): Promise<Decorate extends true ? DecoratedData[] : Data[]> {
  const data: Data[] = await dataFetcher(
    import.meta.glob<{ default: Data }>(`$content/news/*.json`),
    {
      orderBy: ({ date }) => date,
      order: 'desc'
    }
  )()

  if (!decorate) return data as Decorate extends true ? DecoratedData[] : Data[]

  return decorateData(data) as Promise<Decorate extends true ? DecoratedData[] : Data[]>
}

export const decorateData = async (data: Data[]): Promise<DecoratedData[]> => {
  const [events] = await Promise.all([fetchEvents(false)])

  return Promise.all(
    data.map((newsItem) => {
      const images = newsItem.images.map(({ image, caption }) => ({
        image: getImage(image),
        caption
      }))
      const ogImage = newsItem.images[0] ? getOGImage(newsItem.images[0].image) : undefined

      return {
        ...newsItem,
        event: events.find((e) => e.id === newsItem.event),
        images,
        ogImage
      }
    })
  )
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/news/*.jp{,e}g', {
    eager: true,
    query: {
      w: '210;512;700;1400',
      format: 'avif;webp;jpg',
      as: 'picture'
    }
  })
)

export const getOGImage = imageFetcher(
  import.meta.glob<{ default: string }>('$content/news/*.jp{,e}g', {
    eager: true,
    query: {
      w: '1200',
      h: '630',
      fit: 'cover',
      format: 'jpg'
    }
  })
)

export const preSaveHandler: PreSaveEventListener = {
  name: 'preSave',
  collection: collection.name,
  handler: ({
    data: {
      entry: { data }
    }
  }) => {
    return {
      ...data,
      year: new Date((data as unknown as Data).date).getFullYear()
    }
  }
}

export default collection
