import { useMemo, createElement as h, type FC, useEffect, useState } from 'react'
import { type TemplatePreviewProps, useGetMediaAsset } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import type { Data, DecoratedData, Fields } from '.'
import news, { decorateData } from '.'
import Preview from '$components/NewsItemPreview.svelte'
import { dummyPicture } from '$lib/cms/util'
import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { compact } from 'remeda'

export type PreviewData = DecoratedData & {
  socialMedia: Awaited<ReturnType<typeof fetchSocialMedia>>
  contacts: Awaited<ReturnType<typeof fetchContacts>>
}

const EMPTY: PreviewData = {
  id: '',
  slug: '',
  images: [],
  title: '',
  excerpt: '',
  date: '',
  content: '',
  program: [],
  ogImage: '',
  event: undefined,
  contacts: [],
  socialMedia: {
    facebook: '',
    instagram: ''
  }
}

const ReactPreview: FC<TemplatePreviewProps<Data, Fields>> = ({ entry, fields, collection }) => {
  const [decoratedData, setDecoratedData] = useState<PreviewData>(EMPTY)
  const [socialMedia, setSocialMedia] = useState<PreviewData['socialMedia']>(EMPTY.socialMedia)
  const [contacts, setContacts] = useState<PreviewData['contacts']>(EMPTY.contacts)
  const [images, setImages] = useState<{ image: string; caption: string }[]>([])
  const imagesField = useMemo(() => fields.find((field) => field.name === 'images'), [fields])
  const getMediaAsset = useGetMediaAsset(collection, imagesField, entry)

  useEffect(() => {
    fetchSocialMedia().then(setSocialMedia)
    fetchContacts().then(setContacts)
  }, [])

  useEffect(() => {
    Promise.all(
      (entry.data?.images ?? []).map(async ({ image, caption }) => {
        const asset = await getMediaAsset(image)
        return asset ? { image: asset, caption } : null
      })
    ).then((images) => setImages(compact(images)))
  }, [entry.data])

  useEffect(() => {
    if (!entry.data) {
      return
    }

    decorateData([entry.data]).then(([data]) =>
      setDecoratedData({ ...data, socialMedia, contacts })
    )
  }, [entry.data, socialMedia, contacts])

  const props = useMemo(
    (): { data: PreviewData } => ({
      data: {
        ...decoratedData,
        images: images.map(({ image, caption }) => ({ image: dummyPicture(image), caption }))
      }
    }),
    [decoratedData, images]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(news.name, ReactPreview)
}
