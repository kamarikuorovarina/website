import { imagePattern } from '$collections/util'
import { dataFetcher, pictureFetcher } from '$lib/cms/util'
import { fetchData as fetchNews } from '$collections/news'
import type {
  FolderCollection,
  StringOrTextField,
  DateTimeField,
  UUIDField,
  MarkdownField,
  ListField,
  FileOrImageField,
  ObjectField,
  PreSaveEventListener,
  HiddenField
} from '@staticcms/core'
import { filter, first, identity, map, maxBy, minBy, pipe, sortBy } from 'remeda'
import type { Picture } from 'vite-imagetools'
import { isFuture, isToday } from 'date-fns'

export interface Data {
  id: string
  title: string
  image?: string
  description: string
  info: string
  prices: string[]
  instances: EventInstance[]
  slug: string
}

export interface DecoratedData extends Data {
  news_item?: string
}

export type TicketLinks = {
  holvi?: string
  tiketti?: string
}

export interface EventInstance {
  time: string
  place: {
    name: string
    address: string
  }
  ticket_links?: TicketLinks
}

export type FlatEvent = Omit<Data, 'instances'> & EventInstance
export type DecoratedFlatEvent = Omit<DecoratedData, 'instances'> & EventInstance

const id: UUIDField = {
  label: 'ID',
  name: 'id',
  widget: 'uuid',
  allow_regenerate: false
}

const latestTime: HiddenField = {
  label: 'Aika',
  name: 'latest_time',
  widget: 'hidden',
  required: false
}

const year: HiddenField = {
  label: 'Vuosi',
  name: 'year',
  widget: 'hidden',
  required: false
}

const title: StringOrTextField = {
  label: 'Otsikko',
  name: 'title',
  widget: 'text',
  required: true
}

const description: StringOrTextField = {
  label: 'Kuvaus',
  name: 'description',
  widget: 'string',
  required: true
}

const image: FileOrImageField = {
  label: 'Kuva',
  name: 'image',
  widget: 'image',
  required: false,
  pattern: imagePattern
}

const info: MarkdownField = {
  label: 'Info',
  name: 'info',
  widget: 'markdown',
  required: true
}

const prices: ListField = {
  label: 'Hinnat',
  label_singular: 'Hinta',
  name: 'prices',
  widget: 'list',
  required: true,
  fields: [{ label: 'Hinta', name: 'price', widget: 'string', required: true }],
  min: 1
}

const ticketLinks: ObjectField = {
  label: 'Lipunmyyntilinkit',
  name: 'ticket_links',
  widget: 'object',
  required: false,
  fields: [
    {
      label: 'Holvi',
      name: 'holvi',
      widget: 'string',
      required: false,
      pattern: [
        '^https://holvi\\.com/shop/kamarikuorovarina/.*$',
        'Holvi link must match the pattern https://holvi.com/shop/kamarikuorovarina/(...)'
      ]
    },
    {
      label: 'Tiketti',
      name: 'tiketti',
      widget: 'string',
      required: false,
      pattern: [
        '^https://www.tiketti.fi/.+$',
        'Tiketti link must match the pattern https://www.tiketti.fi/...'
      ]
    }
  ]
}

const time: DateTimeField = {
  label: 'Aika',
  name: 'time',
  widget: 'datetime',
  date_format: 'dd.MM.yyyy',
  time_format: 'HH:mm',
  required: true,
  default: ''
}

const place: ObjectField = {
  label: 'Paikka',
  name: 'place',
  widget: 'object',
  required: true,
  fields: [
    { name: 'name', label: 'Nimi', widget: 'string', required: true },
    { name: 'address', label: 'Osoite', widget: 'text', required: true }
  ]
}

const eventFields = [time, place, ticketLinks]
export type EventFields = (typeof eventFields)[number]

const instances: ListField<EventFields> = {
  name: 'instances',
  label: 'Esiintymiset',
  label_singular: 'Esiintyminen',
  widget: 'list',
  fields: eventFields,
  required: true,
  min: 1
}

const fields = [id, title, latestTime, year, image, description, info, prices, instances]
export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'events',
  label: 'Tapahtumat',
  label_singular: 'Tapahtuma',
  folder: 'src/content/events',
  identifier_field: 'id',
  summary_fields: ['title', 'latest_time'],
  sortable_fields: {
    fields: ['latest_time'],
    default: { field: 'latest_time', direction: 'Descending' }
  },
  view_groups: [
    {
      label: 'Vuonna',
      field: 'year'
    }
  ],
  slug: '{{year}}-{{id}}',
  create: true,
  format: 'json',
  media_folder: '',
  public_folder: '/src/content/events',
  fields,
  icon: 'calendar'
}

export function flattenEvents<T extends Data | DecoratedData>(
  events: T[]
): T extends DecoratedData ? DecoratedFlatEvent[] : FlatEvent[] {
  return events.flatMap(({ instances, ...event }) => {
    return instances.map((instance) => {
      return {
        ...event,
        ...instance
      }
    })
  })
}

export async function fetchData<Decorate extends boolean>(
  decorate: Decorate
): Promise<Decorate extends true ? DecoratedData[] : Data[]> {
  const data = await dataFetcher(import.meta.glob<{ default: Data }>(`$content/events/*.json`), {
    orderBy: ({ instances }) =>
      pipe(
        instances,
        map(({ time }) => new Date(time).getTime()),
        minBy(identity),
        (time) => time ?? 0
      ),
    order: 'desc'
  })()

  if (!decorate) return data
  return decorateData(data)
}

export const decorateData = async (data: Data[]): Promise<DecoratedData[]> => {
  const newsItems = await fetchNews(false)

  return data.map((event) => {
    const newsItem = newsItems.find(({ event: id }) => event.id === id)

    return {
      ...event,
      news_item: newsItem?.slug
    }
  })
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/events/*.jp{,e}g', {
    eager: true,
    query: {
      w: '210;512;700;1400',
      format: 'avif;webp;jpg',
      as: 'picture'
    }
  })
)

export const preSaveHandler: PreSaveEventListener = {
  name: 'preSave',
  collection: collection.name,
  handler: ({
    data: {
      entry: { data }
    }
  }) => {
    const latestTime = pipe(
      data?.instances as unknown as EventInstance[],
      map(({ time }) => new Date(time).getTime()),
      maxBy(identity),
      (time) => new Date(time ?? 0)
    )

    return {
      ...data,
      latest_time: latestTime.toISOString(),
      year: latestTime.getFullYear()
    }
  }
}

export const nextEventInstance = (event: Data) =>
  pipe(
    event.instances,
    map(({ time, ...rest }) => ({ ...rest, time: new Date(time) })),
    filter(({ time }) => isToday(time) || isFuture(time)),
    sortBy(({ time }) => time),
    first()
  )

export default collection
