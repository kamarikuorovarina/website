import { useMemo, createElement as h, type FC } from 'react'
import { useMediaAsset, type TemplatePreviewProps } from '@staticcms/core'
import type CMS from '@staticcms/core'
import { useSvelteComponent } from '$lib/cms/preview-helper'
import type { Data, Fields } from '.'
import concerts from '.'
import Preview from '$components/EventPreview.svelte'

const placeholderImage = 'https://via.placeholder.com/300x300'

const EMPTY: Data = {
  id: '',
  slug: '',
  image: '',
  title: '',
  description: '',
  info: '',
  instances: [],
  prices: []
}

const ReactPreview: FC<TemplatePreviewProps<Data, Fields>> = ({ entry, fields, collection }) => {
  const image = useMediaAsset(entry.data?.image, collection, fields, entry) ?? placeholderImage

  const props = useMemo(
    (): { data: Data } => ({ data: { ...EMPTY, ...entry.data, image } }),
    [entry.data, image]
  )

  const div = useSvelteComponent(Preview, props)

  return h('div', { ref: div, id: 'app' })
}

export const registerPreview = (cms: typeof CMS) => {
  cms.registerPreviewTemplate(concerts.name, ReactPreview)
}
