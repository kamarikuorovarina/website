export const imagePattern: [string, string] = [
  '^[a-zA-Z0-9_\\- .()]+.jpe?g$',
  'Kuvan tulee olla jpg eikä sisältää äö tms merkkejä'
]
