import { imagePattern } from '$collections/util'
import { imageFetcher, pictureFetcher } from '$lib/cms/util'
import type {
  FolderCollection,
  StringOrTextField,
  UUIDField,
  FileOrImageField
} from '@staticcms/core'
import { pipe, values, prop, map, shuffle, partition } from 'remeda'
import type { Picture } from 'vite-imagetools'

export interface Data {
  id: string
  first?: boolean
  image: string
  caption: string
  index: number
}

const id: UUIDField = {
  label: 'ID',
  name: 'id',
  widget: 'uuid',
  allow_regenerate: false
}

const image: FileOrImageField = {
  label: 'Kuva',
  name: 'image',
  widget: 'image',
  required: true,
  pattern: imagePattern
}

const caption: StringOrTextField = {
  label: 'Kuvateksti',
  name: 'caption',
  widget: 'string',
  required: true
}

const fields = [id, image, caption]

export type Fields = (typeof fields)[number]

const collection: FolderCollection<Fields> = {
  name: 'choir-images',
  label: 'Kuorogalleria',
  label_singular: 'Kuva',
  folder: 'src/content/choir-images',
  media_folder: '',
  public_folder: '/src/content/choir-images',
  identifier_field: 'caption',
  slug: '{{id}}',
  summary_fields: ['caption'],
  delete: true,
  create: true,
  editor: {
    preview: false
  },
  format: 'json',
  i18n: false,
  fields,
  icon: 'camera'
}

export const fetchData = async () => {
  const data = pipe(
    import.meta.glob<{ default: Data }>('$content/choir-images/*.json', { eager: true }),
    values,
    map(prop('default')),
    shuffle()
  )

  const [[first], rest] = pipe(
    data,
    map((item) => ({ ...item, image: getImage(item.image), ogImage: getOGImage(item.image) })),
    partition(({ first }) => first ?? false)
  )

  return [first, ...rest]
}

export const getImage = pictureFetcher(
  import.meta.glob<{ default: Picture }>('$content/choir-images/*.jp{,e}g', {
    eager: true,
    query: {
      w: '400;800;1024;1440;2000',
      format: 'avif;webp;jpg',
      aspect: '4:3',
      fit: 'cover',
      as: 'picture'
    }
  })
)

export const getOGImage = imageFetcher(
  import.meta.glob<{ default: string }>('$content/choir-images/*.jp{,e}g', {
    eager: true,
    query: {
      w: '1200',
      h: '630',
      fit: 'cover',
      format: 'jpg'
    }
  })
)

export default collection
