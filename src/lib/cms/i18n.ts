import type { Config } from '@staticcms/core'
import type { Locale as DateFnsLocale } from 'date-fns'

import enUS from 'date-fns/locale/en-US'
import fi from 'date-fns/locale/fi'
import sv from 'date-fns/locale/sv'

export type Locale = 'fi' | 'se' | 'en'
export const locales: Locale[] = ['fi', 'se', 'en']
export const defaultLocale: Locale = 'fi'

export const dateFnsLocales: Record<Locale, DateFnsLocale> = {
  fi,
  se: sv,
  en: enUS
}

const config: Config['i18n'] = {
  locales,
  defaultLocale,
  structure: 'multiple_folders'
}

export default config
