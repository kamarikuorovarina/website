import { useEffect, useRef } from 'react'
import type { ComponentType, SvelteComponentTyped } from 'svelte'

export function useSvelteComponent<P extends Record<string, unknown>>(
  Component: ComponentType<SvelteComponentTyped<P>>,
  props?: P
) {
  const div = useRef<HTMLDivElement>(null)
  const preview = useRef<SvelteComponentTyped<P> | null>(null)

  useEffect(() => {
    if (div.current) {
      preview.current = new Component({ target: div.current, props })
    }

    return () => preview.current?.$destroy()
  }, [div.current])

  useEffect(() => {
    if (!props || !preview.current) {
      return
    }

    preview.current.$set(props)
  }, [props, preview.current])

  return div
}
