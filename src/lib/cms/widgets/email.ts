import { base64decode, base64encode } from '$lib/util'
import {
  type WidgetControlProps,
  type WidgetPreviewProps,
  type StringOrTextField,
  type BaseField,
  StringControl,
  StringPreview
} from '@staticcms/core'
import { type FC, createElement as h } from 'react'

export interface EmailField extends BaseField {
  widget: 'email'
}

export const EmailControl: FC<WidgetControlProps<string, StringOrTextField>> = (props) => {
  const { value, onChange } = props

  const decodedValue = value ? base64decode(value) : value

  const handleChange = (value: string) => {
    onChange(base64encode(value))
  }

  return h(StringControl, { ...props, value: decodedValue, onChange: handleChange })
}

export const EmailPreview: FC<WidgetPreviewProps<string, StringOrTextField>> = (props) => {
  const { value } = props
  const decodedValue = value ? base64decode(value) : value

  return h(StringPreview, { ...props, value: decodedValue })
}
