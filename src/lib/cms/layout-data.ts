import { fetchData as fetchContacts } from '$collections/contacts'
import { fetchData as fetchSocialMedia } from '$collections/settings/social-media'
import { fetchLink as fetchChoirLink } from '$collections/translated-pages/choir'
import { fetchLink as fetchConcertsLink } from '$collections/pages/concerts'
import { fetchLink as fetchListenLink } from '$collections/pages/listen'
import { fetchLink as fetchRauhaaLink } from '$collections/pages/rauhaa'
import { fetchLink as fetchHighlightsLink } from '$collections/pages/past-events'
import { fetchLink as fetchPressLink } from '$collections/pages/press'
import { fetchLink as fetchNewsLink } from '$collections/pages/news'

export const fetchLayoutData = async (locale?: string) => {
  const [contacts, socialMedia, ...menu] = await Promise.all([
    fetchContacts(locale),
    fetchSocialMedia(),
    fetchNewsLink(),
    fetchChoirLink(locale),
    fetchConcertsLink(),
    fetchListenLink(),
    fetchRauhaaLink(),
    fetchHighlightsLink(),
    fetchPressLink()
  ])

  return {
    contacts,
    socialMedia,
    menu
  }
}
