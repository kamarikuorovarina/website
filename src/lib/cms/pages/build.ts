import { formatDistanceToNow } from 'date-fns'
import { createElement as h, type FC, useEffect, useState, useCallback } from 'react'

interface Job {
  status: string
  created_at: string
  commit: {
    author_name: string
    committed_date: string
    short_id: string
    title: string
  }
}

export const Build: FC = () => {
  const user = localStorage.getItem('static-cms-user')
  const token = user && JSON.parse(user).token
  const [pipelineTriggerToken, setPipelineTriggerToken] = useState(
    localStorage.getItem('pipeline-trigger-token')
  )
  const [job, setJob] = useState<Job | null>(null)
  const [version, setVersion] = useState('')

  const setToken = (token: string) => {
    localStorage.setItem('pipeline-trigger-token', token)
    setPipelineTriggerToken(token)
  }

  const triggerBuild = useCallback(() => {
    const triggerToken = pipelineTriggerToken || prompt('Enter token')
    if (!triggerToken) {
      return
    }

    if (triggerToken !== pipelineTriggerToken) {
      setToken(triggerToken)
    }

    fetch(`https://gitlab.com/api/v4/projects/kamarikuorovarina%2Fwebsite/trigger/pipeline`, {
      method: 'POST',
      body: new URLSearchParams({
        token: triggerToken,
        ref: 'main',
        'variables[TRIGGER]': 'true'
      })
    })
  }, [pipelineTriggerToken])

  useEffect(() => {
    const interval = setInterval(() => {
      fetch('https://gitlab.com/api/v4/projects/kamarikuorovarina%2Fwebsite/jobs', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then(async (res) => {
        const jobs = await res.json()
        setJob(jobs[0])
      })

      fetch('https://kamarikuorovarina.fi/version.json').then(async (res) => {
        const version = await res.text()
        console.log(version)
        setVersion(version)
      })
    }, 1000)

    return () => clearInterval(interval)
  }, [token])

  return h('div', {
    children: [
      h('h1', { children: 'Build', className: 'text-4xl font-bold mb-4' }),
      h('p', {
        children: [
          'Build status: ',
          job?.status,
          job?.status === 'running' && ` since ${formatDistanceToNow(new Date(job.created_at))}`
        ],
        className: 'mb-4'
      }),
      job?.status === 'running' &&
        h('pre', {
          children: JSON.stringify(
            {
              ref: job.commit.short_id,
              timestamp: job.commit.committed_date,
              title: job.commit.title,
              author: job.commit.author_name
            },
            null,
            2
          ),
          className: 'mb-4'
        }),
      h('div', {
        children: [h('p', { children: 'Online version:' }), h('pre', { children: version })],
        className: 'mb-4'
      }),
      h('button', {
        disabled: !job || ['running', 'pending'].includes(job.status),
        children: 'Trigger build',
        onClick: triggerBuild,
        className: 'btn btn-contained-primary'
      })
    ]
  })
}
