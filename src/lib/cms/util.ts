import { filter, identity, map, pipe, reverse, sortBy } from 'remeda'
import type { Picture } from 'vite-imagetools'
import { defaultLocale, locales, type Locale } from './i18n'

const isValidLocale = (locale?: string): locale is Locale => {
  return locale ? locales.includes(locale as Locale) : false
}

export const sanitizeLocale = (locale?: string): Locale => {
  return isValidLocale(locale) ? locale : defaultLocale
}

interface FetchOptions<T> {
  orderBy?: (item: T) => number | string | boolean
  order?: 'asc' | 'desc'
  filter?: (item: T) => boolean
}

export function i18nDataFetcher<T>(
  dataFiles: Record<Locale, Record<string, () => Promise<{ default: T }>>>,
  options: FetchOptions<T> = {}
): (locale?: string) => Promise<T[]> {
  return (locale?: string) => dataFetcher(dataFiles[sanitizeLocale(locale)], options)()
}

const basename = (path: string) => path.split('/').pop()!.split('.').shift()!

export function dataFetcher<T>(
  dataFiles: Record<string, () => Promise<{ default: T }>>,
  { orderBy, order, filter: filterFn }: FetchOptions<T & { slug: string }> = {}
): () => Promise<(T & { slug: string })[]> {
  return async () => {
    const modules = await Promise.all(
      Object.entries(dataFiles).map(([path, file]) =>
        Promise.all([basename(path), file()] as const)
      )
    )

    return pipe(
      modules,
      map(([slug, module]) => ({ ...module.default, slug })),
      orderBy ? sortBy(orderBy) : identity,
      order === 'desc' ? reverse() : identity,
      filterFn ? filter(filterFn) : identity
    )
  }
}

export const dummyPicture = (src: string): Picture => ({
  img: { src, w: 0, h: 0 },
  sources: {}
})

export function pictureFetcher(
  images: Record<string, { default: Picture }>
): (path: string) => Picture {
  return (path: string) => {
    if (!path.startsWith('/src')) {
      return dummyPicture(path)
    }

    let image = images[path]
    if (!image) {
      image = { default: dummyPicture('') }
      console.error(`Image ${path} not found, available images: ${Object.keys(images).join(', ')}`)
    }

    return image.default
  }
}

export function imageFetcher(
  images: Record<string, { default: string }>
): (path: string) => string {
  return (path: string) => {
    if (!path.startsWith('/src')) {
      return path
    }

    let image = images[path]
    if (!image) {
      image = { default: '' }
      console.error(`Image ${path} not found, available images: ${Object.keys(images).join(', ')}`)
    }

    return image.default
  }
}
