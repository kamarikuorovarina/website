export type SingleFileTranslated<T> = {
  [locale: string]: T
}
