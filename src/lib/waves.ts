export enum Color {
  sky = 'sky',
  slate = 'slate',
  silver = 'silver',
  mustard = 'mustard',
  cottoncandy = 'cottoncandy'
}

export interface WaveDefinition {
  color: Color
  xOffsetTop: number
  xOffsetBottom: number
  align?: 'left' | 'right' | 'center'
}

export const choir: WaveDefinition = {
  color: Color.sky,
  xOffsetTop: 500,
  xOffsetBottom: 500 + Math.PI
}

export const concerts: WaveDefinition = {
  color: Color.cottoncandy,
  xOffsetTop: 300,
  xOffsetBottom: 500
}

export const rauhaa: WaveDefinition = {
  color: Color.silver,
  xOffsetTop: 7,
  xOffsetBottom: 5.2
}

export const press: WaveDefinition = {
  color: Color.silver,
  xOffsetTop: 3.8,
  xOffsetBottom: 5.8
}

export const listen: WaveDefinition = {
  color: Color.mustard,
  xOffsetTop: 1.7,
  xOffsetBottom: 3.4,
  align: 'center'
}

export const pastEvents: WaveDefinition = {
  color: Color.cottoncandy,
  xOffsetTop: 3.3,
  xOffsetBottom: 0.6
}

export const news: WaveDefinition = {
  color: Color.mustard,
  xOffsetTop: 0.0,
  xOffsetBottom: 5.0,
  align: 'right'
}

export default {
  choir,
  concerts,
  rauhaa,
  press,
  listen,
  pastEvents
}
