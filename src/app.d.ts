// See https://kit.svelte.dev/docs/types#app

import type { Locale } from '$lib/cms/i18n'
import type { WaveDefinition } from './lib/waves'

// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    interface PageData {
      wave?: WaveDefinition
      locale?: Locale
      menu?: { title: string; href: string }[]
      title?: string
      description?: string
      image?: string
      url?: string
    }
    // interface Platform {}
  }
}

export {}
