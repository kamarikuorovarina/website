import type { Config } from 'tailwindcss'

export enum Color {
  sky = 'sky',
  slate = 'slate',
  silver = 'silver',
  mustard = 'mustard',
  cottoncandy = 'cottoncandy'
}

export const colors: Record<Color, string> = {
  sky: '#3CB4F0',
  slate: '#41465A',
  silver: '#BEBEBE',
  mustard: '#F0CD78',
  cottoncandy: '#FA8CBE'
}

const config: Config = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  variants: {
    extend: {
      opacity: ['group-hover']
    }
  },
  theme: {
    extend: {
      colors,
      scale: {
        '85': '.85'
      },
      gridRow: {
        'span-7': 'span 7 / span 7',
        'span-8': 'span 8 / span 8'
      },
      gridRowStart: {
        '8': '8',
        '9': '9',
        '10': '10',
        '11': '11',
        '12': '12',
        '13': '13',
        '14': '14',
        '15': '15',
        '16': '16',
        '17': '17',
        '18': '18',
        '19': '19',
        '20': '20',
        '21': '21',
        '22': '22',
        '23': '23',
        '24': '24'
      },
      width: {
        '100': '34rem'
      },
      fontFamily: {
        sans: ['Tajawal', 'ui-sans-serif', 'system-ui'],
        serif: ['EB Garamond', 'ui-serif', 'Georgia']
      },
      backgroundSize: {
        '100': '100%',
        '75': '75%',
        '60': '60%',
        '50': '50%',
        '25': '25%'
      },
      transitionProperty: {
        height: 'height'
      }
    }
  },
  plugins: []
}

export default config
