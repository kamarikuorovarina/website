import { sveltekit } from '@sveltejs/kit/vite'
import { imagetools } from 'vite-imagetools'
import type { UserConfig } from 'vite'
import theme from './src/lib/tailwind'

const config: UserConfig = {
  plugins: [sveltekit(), imagetools({ removeMetadata: true })],
  test: {
    include: ['src/**/*.{test,spec}.{js,ts}']
  },
  define: {
    TAILWIND_THEME: JSON.stringify(theme)
  },
  server: {
    host: process.env.HOST || 'localhost'
  }
}

export default config
